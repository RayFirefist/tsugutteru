import os 

print("Tsugutteru loaded!")

def modify(chunks):
    """
    chunks is a generator that can be used to iterate over all chunks.
    """
    for chunk in chunks:
        yield chunk.replace(bytes("AsNeeded", "ascii"), bytes("StartApp", "ascii"))

    file = open("temp", "wb")
    file.write(chunk)
    file.close()


def responseheaders(flow):
    if not(str(flow.request.url).find("AssetBundleInfo") == -1):
        print("Japanese/WorldWide AssetBundleInfo detected!")
        flow.response.stream = modify