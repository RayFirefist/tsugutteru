# Tsugutteru

## Tsugutteru (made by RayFirefist)

Small script for download all data on BanG Dream clients via editing `AsNeeded` parts as `StartApp`.
Compatible with iOS and Android.

## Preparing

Linux (pure or W10):
```bash
apt install mitmproxy
```

macOS (not tested yet):
```bash
brew install mitmproxy
```

### READ THIS BEFORE LOAD TSUGUTTERU
Before you start the tool, take your target device, go to mitm.it and choose the platform which you are using. Since BanDori uses HTTPS connections it's mandatory this step.

Windows:
[Download the Windows binaries](https://mitmproxy.org/)

## How to use

* Load Tsugutteru using one of the following ways:
    * Linux/macOS(?): `mitmproxy -p 8080 -s tsugutteru.py`
    - Windows: `mitmdump -p 8080 -s tsugutteru.py`
* Know your local IP Address.
* Set up the proxy data on your device.
* Load the game on your device.
* When you see the download mode you can unset your proxy data on your device.

Make sure that you are able to download any files in 5 seconds since the game's timeout of download is 5 seconds.

[Credits for base script click here](https://github.com/mitmproxy/mitmproxy/blob/master/examples/complex/stream_modify.py)